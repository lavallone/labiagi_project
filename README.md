# LABIAGI_Project
# Progetto per il Laboratorio di Intelligenza Artificiale e Grafica Interattiva

Premessa: in questa repo non inserisco il workspace utilizzato (quello fornito dal prof. Grisetti con l'ecosistema srrg incluso) poichè
non avrebbe senso. Ognuno si configura il workspace a proprio piacimento. Detto ciò, i comandi che utilizzeremo sono specifici del workspace ROS utilizzato.
Quindi nella cartella ROS è presente solo il package che ho implementato per il progetto.

Come usare il codice:

1.  eseguire il seguente comando da terminale per essere in grado di far partire tutte le componenti dello stack di navigazione e simulazione: 

    > cd ~/Desktop/workspaces/RP_ws/srrg2_workspace/src/srrg2_configs/navigation_2d
    > ../../../../srrg2_webctl/proc_webctl run_navigation.webctl
    
    - esegui prima di tutto roscore da terminale, perchè ci impiega tanto su vm. In caso lancialo sull'interfaccia web che abbiamo fatto 		partire poc'anzi. Ovviamente questi comandi funzionano con il setup del workspace che abbiamo utilizzato nel corso.
    Per capire quali comandi avremmo dovuto lanciare uno alla volta, fare riferimento al file nav_setup_steps.txt della repo.

2.  appena si apre rviz andiamo a fissare la posizione iniziale del robot (il localizer deve essere ovviamente attivo)

3.  successivamente eseguiamo il comando per attivare ros_bridge:
    > roslaunch rosbridge_server rosbridge_websocket.launch

4.  attiviamo il nodo ROS reach_goal_node del package reach_goal attraverso il comando:
    > rosrun reach_goal reach_goal_node

5.  infine, dentro la cartella /src della repo eseguiamo il comando per far partire l'interfaccia grafica:
    > python3 GUI_login.py
