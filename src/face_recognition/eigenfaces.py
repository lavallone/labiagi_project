from sklearn.preprocessing import LabelEncoder
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from skimage.exposure import rescale_intensity
from pyimagesearch.faces import load_face_dataset
from imutils import build_montages
import numpy as np
import imutils
import time
import cv2
import os

def face_recognition(path_input_images, path_test_image):

	# load our serialized face detector model from disk
	print("[INFO] loading face detector model...")
	prototxtPath = "/home/leo/Desktop/PROJECT/labiagi_project/src/face_recognition/face_detector/deploy.prototxt"
	weightsPath = "/home/leo/Desktop/PROJECT/labiagi_project/src/face_recognition/face_detector/res10_300x300_ssd_iter_140000.caffemodel"
	net = cv2.dnn.readNet(prototxtPath, weightsPath) # mi salvo la rete neurale

	# load the CALTECH faces dataset
	print("[INFO] loading dataset...")
	test_image = cv2.imread(path_test_image)
	test_image2 = test_image.copy()
	(faces, labels, test_image_index, test_image, test_image_2, x1 ,x2, y1, y2) = load_face_dataset(test_image, test_image2, path_input_images, net, minConfidence=0.5, minSamples=20) # le persone con meno di 20 foto non vengono considerate
	print("[INFO] {} images in dataset".format(len(faces)))
	# faces è la lista delle immagini che derivano dalla face detection, mentre labels è la lista dei nomi delle persone delle immagini

	# flatten all 2D faces into a 1D list of pixel intensities
	pcaFaces = np.array([f.flatten() for f in faces]) # ogni faccia trovata diventa un unico vettore da 2914 elementi

	# encode the string labels as integers --> mappa i nomi delle persone con dei numeri
	le = LabelEncoder()
	labels = le.fit_transform(labels) # i labels vanno da [0, 4] inclusi

	# io mi voglio prendere la test_image_index-esima immagine per farne il riconoscimento
	origTest = np.array([ faces[test_image_index] ])
	testX = np.array([ pcaFaces[test_image_index] ])
	testY = np.array([ labels[test_image_index] ])

	# elimino la test_image dal set delle training images
	origTrain = np.delete(faces, 0 , axis=0 )
	trainX = np.delete(pcaFaces, test_image_index , axis=0)
	trainY = np.delete(labels, test_image_index)

	cv2.imshow("Face detection", test_image)
	cv2.waitKey(1000)
	cv2.destroyAllWindows()

	# compute the PCA (eigenfaces) representation of the data, then project the training data onto the eigenfaces subspace
	print("[INFO] creating eigenfaces...")
	pca = PCA( svd_solver="randomized", n_components=50, whiten=True)
	start = time.time()
	trainX = pca.fit_transform(trainX)
	end = time.time()
	print("[INFO] computing eigenfaces took {:.4f} seconds".format(end - start))

	# train a classifier on the eigenfaces representation
	print("[INFO] training classifier...")
	model = SVC(kernel="rbf", C=10.0, gamma=0.001, random_state=42)
	model.fit(trainX, trainY) # stiamo preparando le eigen faces per essere confrontate con la input image 

	# predict our test_image
	print("[INFO] predicting test image...")
	predictions = model.predict( pca.transform(testX))

	# grab the predicted name and actual name
	predName = le.inverse_transform([predictions[0]])[0] # è il nome che l'algoritmo ha riconosciuto 
	actualName = le.classes_[testY[0]] # è il nome vero della persona 

	face = np.dstack([origTest[0]] * 3)
	face = imutils.resize(face, width=250)
	cv2.putText(face, "Eigen faces recognition...", (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
	face = cv2.resize(face, (x2-x1, y2-y1))

	res_im = test_image.copy()
	res_im[y1:y2 , x1:x2 ,:] = face
	cv2.imshow("Face recognition", res_im)
	cv2.waitKey(1000)
	cv2.destroyAllWindows()

	cv2.imshow("Face recognized", test_image2)
	cv2.waitKey(1000)
	cv2.destroyAllWindows()

	print("[INFO] Professor recognized")

	# display the predicted name and actual name
	# if predName == actualName:
	# 	print("[INFO] {} riconosciuto CORRETTAMENTE".format( predName))
	# else:
	# 	print("[INFO] {} NON riconosciuto".format( predName))

	return predName
