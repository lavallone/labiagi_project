from tkinter import *
from tkinter import ttk
from PIL import ImageTk, Image
import sqlite3
from tkinter import messagebox
from datetime import datetime
import time
from threading import Thread
import globals
import roslibpy
from delivery_system import ros_bridge_1
from delivery_system import ros_bridge_2

class MyThread_1 (Thread):
    def __init__(self, x, y, root_user, client):
        Thread.__init__(self)
        self.x = x
        self.y = y
        self.root_user = root_user
        self.client = client
    def run(self):
        ros_bridge_1(self.x, self.y, self.root_user, self.client)

class MyThread_2 (Thread):
    def __init__(self, x, y, id, root_user, client):
        Thread.__init__(self)
        self.x = x
        self.y = y
        self.id = id
        self.root_user = root_user
        self.client = client
    def run(self):
        ros_bridge_2(self.x, self.y, self.id, self.root_user, self.client)

def show_map(map_image):

    top_map = Toplevel()
    top_map.title("DIAG map")
    my_label = Label(top_map, image=map_image).pack()

def show_users(root_user, canvas, id):

    conn = sqlite3.connect("project.db")
    c = conn.cursor()   
    c.execute("SELECT ID, name FROM current_users WHERE ID !='"+str(id)+"'")
    record = c.fetchall()

    print_records=""
    for r in record:
        print_records += "User ID "+str(r[0])+" (Prof. "+r[1]+")\n"

    conn.commit()
    conn.close()
        
    label_users = Label(root_user, text=print_records, pady=20, bg="white", fg="black", font="Future", width=21, height=3)
    w3 = canvas.create_window(253, 156, anchor=NW, window=label_users)

def logout(root_user, id):

    response = messagebox.askquestion("User 000"+str(id), "Are you sure to logout?", parent=root_user)
   
    if response == "yes":
        conn = sqlite3.connect("project.db")
        c = conn.cursor()
        c.execute("DELETE from current_users WHERE ID =='"+str(id)+"'")
        conn.commit()
        conn.close()

        root_user.destroy()
    else:
        return

def process_request(root_user, canvas, x, y, id, b_order, client):

    if globals.isFree == 1: # se il robot non è occupato
        globals.isFree = 0 # comunico agli utenti che il robot è occupato
        l = Label(root_user, text="", pady=20, bg="white", width=35, height=2)
        ll = Label(root_user, text="How many?", font="Future", bg="white", width=12, height=1)
        def f1():
            canvas.delete(w6)
            canvas.delete(w7)
            canvas.delete(w8)
            canvas.delete(w9)
            b_order["state"] = DISABLED
            thread = MyThread_1(x, y, root_user, client)
            thread.start()
            b_order["state"] = ACTIVE
        def f2():
            canvas.delete(w6)
            canvas.delete(w7)
            canvas.delete(w8)
            canvas.delete(w9)
            b_order["state"] = DISABLED
            thread = MyThread_2(x, y, id, root_user, client)
            thread.start()
            b_order["state"] = ACTIVE
        b_1 = Button(root_user, text="1", padx=20, bg="#800000", fg="white", font="Future", command=f1)
        b_2 = Button(root_user, text="2", padx=20, bg="#800000", fg="white", font="Future", command=f2)
        w6 = canvas.create_window(120, 285, anchor=NW, window=l)
        w7 = canvas.create_window(265, 297, anchor=NW, window=b_1)
        w8 = canvas.create_window(336, 297, anchor=NW, window=b_2)
        w9 = canvas.create_window(130, 302, anchor=NW, window=ll)
    else: 
        messagebox.showinfo("INFO for user 000"+str(id), "ROBOT IS BUSY...", parent=root_user)

def open_user_window(name, id, client):

    now = datetime.now()
    now_string = now.strftime("%d/%m/%Y %H:%M:%S")
    d = now_string[:10]
    t = now_string[11:] 

    root_user = Toplevel()
    root_user.geometry("600x365")
    root_user.title("User ID 000"+str(id))
    bg_path = "/home/leo/Desktop/PROJECT/labiagi_project/src/data/user_image.jpg"
    bg = ImageTk.PhotoImage(Image.open(bg_path))

    canvas = Canvas(root_user, width=1400, height=800)
    canvas.pack(fill="both", expand=True)
    canvas.create_image(0, 0, image=bg, anchor=NW)

    conn = sqlite3.connect("project.db")
    c = conn.cursor()   
    c.execute("INSERT INTO current_users VALUES (:ID, :name)", { "ID" : id, "name" : name }) # aggiorno gli utenti che sono attualmente connessi al servizio
    c.execute("SELECT room,x,y FROM assigned_rooms WHERE ID =='"+str(id)+"'")
    record = c.fetchall()
    room = record[0][0]
    x = record[0][1]
    y = record[0][2]
    conn.commit()
    conn.close()

    canvas.create_text(115, 20, text="Welcome Prof. "+name, font=("Future", 15), fill="#800000")
    canvas.create_text(45, 50, text="ID 000"+str(id), font=("Future", 15), fill="#800000")
    canvas.create_text(50, 80, text="Room "+room, font=("Future", 15), fill="#800000")
    canvas.create_text(70, 115, text=d+"  "+t, font=("Future", 10), fill="black")
    canvas.create_text(260, 267, text="Do you want coffee delivered?", font=("Future", 20), fill="#800000")

    map_image = ImageTk.PhotoImage(Image.open("/home/leo/Desktop/PROJECT/labiagi_project/src/data/diag_map.jpg"))
    b_map = Button(root_user, text="Show map", padx=20, bg="#800000", fg="white", font="Future", command=lambda : show_map(map_image))
    b_users = Button(root_user, text="Other users logged in", padx=20, bg="#800000", fg="white", font="Future", command=lambda : show_users(root_user, canvas, id))
    b_logout = Button(root_user, text="Logout", padx=20, bg="#800000", fg="white", font="Future", command=lambda : logout(root_user, id))
    b_order = Button(root_user, text="MAKE THE ORDER", padx=20, bg="white", fg="#800000", font="Future", command=lambda : process_request(root_user, canvas, x, y, id, b_order, client))

    w1 = canvas.create_window(469, 124, anchor=NW, window=b_map) 
    w2 = canvas.create_window(252, 124, anchor=NW, window=b_users)
    w4 = canvas.create_window(498, 331, anchor=NW, window=b_logout) 
    w5 = canvas.create_window(165, 297, anchor=NW, window=b_order)

    root_user.mainloop()