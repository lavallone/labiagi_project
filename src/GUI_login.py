from tkinter import *
from tkinter import ttk
from PIL import ImageTk, Image
from tkinter import filedialog
from tkinter import messagebox
import sqlite3
import os
import roslibpy

import sys
sys.path.append("/home/leo/Desktop/PROJECT/labiagi_project/src/face_recognition/")
from eigenfaces import *

from user_GUI import open_user_window
import globals

####
root = Tk()
client=roslibpy.Ros(host='localhost', port=9090, is_secure=False)
prof_name = ""
l_name = Label(root)
####

def face_recognition_callback(root):
    global prof_name
    global l_name

    root.filename = filedialog.askopenfilename(title="Simulating face recognition", initialdir="/home/leo/Desktop/PROJECT/labiagi_project/src/face_recognition/professor_faces", filetypes=(("jpg files", "*.jpg"),("all files", "*.*")))
    prof_name = face_recognition("/home/leo/Desktop/PROJECT/labiagi_project/src/face_recognition/professor_faces", root.filename)

    l_name = Label(frame, text="Salve "+prof_name+", immetta la password: ", padx=20, pady=20, bg="white", font="Future")
    l_name.grid(row=1, column=0, columnspan=3)
    l1.grid_forget()
    b1.grid_forget()
    l2.grid(row=2, column=0,  columnspan=2)
    t.grid(row=2, column=2)

# gestione inserimento password
def press_enter_button_callback(event):
    global prof_name
    global l_name

    written_passwd = t.get()
    t.delete(0, 'end')
    already_loggedin = False

    conn = sqlite3.connect("project.db")
    c = conn.cursor()   
    name = prof_name[6:]
    c.execute("SELECT ID FROM assigned_rooms WHERE name =='"+name+"'")
    record = c.fetchall()
    id = record[0][0]

    c.execute("SELECT password FROM password_users WHERE ID =='"+str(id)+"'")
    record = c.fetchall()
    passwd_db = record[0][0]

    c.execute("SELECT * FROM current_users WHERE ID =='"+str(id)+"'")
    record = c.fetchall()
    if record != []:
        already_loggedin = True 
    conn.commit()
    conn.close()

    if written_passwd != passwd_db:
        messagebox.showerror("ERROR", "Wrong Password!!!\nCannot access to the system")
        l2.grid_forget()
        t.grid_forget()
        l_name.grid_forget()

        l1.grid(row=1, column=0, columnspan=2)
        b1.grid(row=1, column=2)

    else: # apriamo la finestra utente
        if already_loggedin == True:
            messagebox.showinfo("INFO", "USER ALREADY LOGGED IN!!!")
            l2.grid_forget()
            t.grid_forget()
            l_name.grid_forget()

            l1.grid(row=1, column=0, columnspan=2)
            b1.grid(row=1, column=2)
            return

        l2.grid_forget()
        t.grid_forget()
        l_name.grid_forget()

        l1.grid(row=1, column=0, columnspan=2)
        b1.grid(row=1, column=2)

        open_user_window(name, id, client)


if __name__ == '__main__':

    globals.initialize() #inizializzo le variabili globali comuni a tutti i file della directory

    ##########################################################################################################
    # We connect to ROS_BRIDGE
    client.run()
    print("[Connected to ROS]")

    def f(msg):
        if msg['data'] == 1:
            globals.isAtGoal = 1
        elif msg['data'] == 2:
            globals.isAtBase = 1
        elif msg['data'] == 3:
            globals.isAtOtherGoal = 1
    sub = roslibpy.Topic(client, "/reach_goal/arrived", "std_msgs/Int8")
    sub.subscribe(f)

    ##########################################################################################################

    root.geometry("1000x521")
    root.title("LOGIN")

    bg = ImageTk.PhotoImage(Image.open("/home/leo/Desktop/PROJECT/labiagi_project/src/data/login_image.jpg"))

    canvas = Canvas(root, width=1400, height=800)
    canvas.pack(fill="both", expand=True)
    canvas.create_image(0, 0, image=bg, anchor=NW)

    frame = LabelFrame(root, text="Login", padx=20, pady=20, bg="white", relief=RAISED, font="Future")
    l1 = Label(frame, text="Face recognition", padx=20, pady=20, bg="white", fg="#800000", font="Future")
    b1 = Button(frame, text="Look into the camera", padx=20, bg="#800000", fg="white", font="Future", command=lambda : face_recognition_callback(root))
    l2 = Label(frame, text="Password", pady=20, bg="white", fg="#800000", font="Future")
    t = Entry(frame, show="*", width=15)

    l1.grid(row=1, column=0, columnspan=2)
    b1.grid(row=1, column=2)

    window = canvas.create_window(40, 210, anchor=NW, window=frame) 

    root.bind('<Return>', press_enter_button_callback) 

    root.mainloop()