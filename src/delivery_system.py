import roslibpy
from tkinter import *
from tkinter import messagebox
import time
import globals
import sqlite3
import threading

def ros_bridge_1(x, y, root_user, client):

    messagebox.showinfo("INFO","Making coffee...", parent=root_user)
    pub = roslibpy.Topic(client, "/reach_goal/goal", "reach_goal/Goal")
    pub.publish(roslibpy.Message({'x' : x, 'y' : y, 'numCoffee' : 1}))

    while True:
        if globals.isAtGoal == 1:
            messagebox.showinfo("INFO","1 Coffee is arrived", parent=root_user)
            print("[INFO] Coffee is arrived")
            globals.isAtGoal = 0
        elif globals.isAtBase == 1:
            messagebox.showinfo("INFO","Robot is at the coffee machine, ready for new orders", parent=root_user)
            print("[INFO] Robot is at the coffee machine")
            print("[INFO] Exiting from ros_bridge_1 "+str(threading.currentThread().name))
            pub.unadvertise()
            globals.isAtBase = 0
            globals.isFree = 1 # ora il robot diventa di nuovo libero
            break


def ros_bridge_2(x, y, id, root_user, client): ## ancora da aggiustare

    messagebox.showinfo("INFO","Making coffee...", parent=root_user)
    pub_1 = roslibpy.Topic(client, "/reach_goal/goal", "reach_goal/Goal")
    pub_1.publish(roslibpy.Message({'x' : x, 'y' : y, 'numCoffee' : 2}))

    pub_2 = roslibpy.Topic(client, "/reach_goal/other_goal", "reach_goal/OtherGoal")

    other_id=0
    while (True):
        if globals.isAtGoal == 1:
            messagebox.showinfo("INFO","2 Coffees are arrived", parent=root_user)
            print("[INFO] Coffee is arrived")
            response = messagebox.askquestion("QUESTION","Do you still want 2 coffees?", parent=root_user)
            if response == "yes":
                messagebox.showinfo("INFO","Robot is going back to the coffee machine", parent=root_user)
                print("[INFO] User 000"+str(id)+" still wants 2 coffees")
                pub_2.publish(roslibpy.Message({'x' : 51.16, 'y' : 11.05, 'isGoingToOther' : 0}))
            else: # prendo uno degli user già loggati e gli regalo un caffè 
                messagebox.showinfo("INFO","Give one coffee to one of the other users logged in", parent=root_user)
                print("[INFO] User 000"+str(id)+" doesn't want anymore 2 coffees")
                conn = sqlite3.connect("project.db")
                c = conn.cursor()   
                c.execute("SELECT ID FROM current_users WHERE ID !='"+str(id)+"'")
                record = c.fetchall()
                if record == []: # se sono l'unico utente loggato
                    messagebox.showinfo("INFO","There are not other users logged in, so you drink 2 coffees anyway", parent=root_user)
                    print("[INFO] NO OTHER USERS, user 000"+str(id)+" drinks 2 coffees anyway")
                    pub_2.publish(roslibpy.Message({'x' : 51.16, 'y' : 11.05, 'isGoingToOther' : 0}))
                else :
                    other_id = record[0][0]
                    c.execute("SELECT x,y FROM assigned_rooms WHERE ID =='"+str(record[0][0])+"'")
                    record = c.fetchall()
                    conn.commit()
                    conn.close()
                    pub_2.publish(roslibpy.Message({'x' : record[0][0], 'y' : record[0][1], 'isGoingToOther' : 1}))
            globals.isAtGoal = 0
        elif globals.isAtBase == 1:
            messagebox.showinfo("INFO","Robot is at the coffee machine, ready for new orders", parent=root_user)
            print("[INFO] Robot is at the coffee machine")
            print("[INFO] Exiting from ros_bridge_2 "+str(threading.currentThread().name))
            pub_1.unadvertise()
            pub_2.unadvertise()
            globals.isAtBase = 0
            globals.isFree = 1 # ora il robot diventa di nuovo libero
            break
        elif globals.isAtOtherGoal == 1:
            messagebox.showinfo("INFO","Robot delivered your additional coffee to the user 000"+str(other_id), parent=root_user)
            print("[INFO] Robot is arrived to the other user's office")
            globals.isAtOtherGoal = 0