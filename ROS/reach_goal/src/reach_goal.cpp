#include <ros/ros.h>
#include <reach_goal/Goal.h>
#include <reach_goal/OtherGoal.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/Int8.h>
#include <nav_msgs/Odometry.h>
#include <cmath>
#include <math.h>
#include <vector>
#include <tf/tf.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_msgs/TFMessage.h>


using namespace std;

tf2_ros::Buffer tfBuffer;

// states
int cruising = 0;
int returning=0;
int goingOtherGoal=0;

int seq = 0;
int numCoffee=0;

float current_position[2];
float target_position[2];
float other_target_position[2];
float base_position[2]={51.16 ,11.05};

ros::Publisher pub;
ros::Subscriber goal_sub;
ros::Subscriber pos_sub;

ros::Publisher arrived_pub;
ros::Subscriber other_goal_sub;

void pub_goal_on_ros(float x, float y){
    geometry_msgs::PoseStamped msg;
    msg.header.seq = seq++;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = "map";

    msg.pose.position.x = x;
    msg.pose.position.y = y;
    msg.pose.position.z = 0.0;

    msg.pose.orientation.x = 0.0;
    msg.pose.orientation.y = 0.0;
    msg.pose.orientation.z = 0.0;
    msg.pose.orientation.w = 1.0;

    pub.publish(msg);
}

void GoalCallback(const reach_goal::GoalConstPtr& goal){ // è quando arriva questo messaggio che inizia tutto

    float x=goal->x;
    float y=goal->y;
    numCoffee = goal->numCoffee;
    target_position[0]=x;
    target_position[1]=y;
    ROS_INFO("Office coordinates: [x: %f, y: %f]", x, y);

    pub_goal_on_ros(x,y);

    cruising=1;

}

void OtherGoalCallback(const reach_goal::OtherGoalConstPtr& other_goal){ // è quando arriva questo messaggio che inizia tutto

    float x=other_goal->x;
    float y=other_goal->y;
    int isGoingToOther = other_goal->isGoingToOther;
    if (isGoingToOther == 0){ // e quindi l'utente specifico si vuole bere entrambi i caffè (il robot ritorna alla base)
        ROS_INFO("User decides to drink 2 coffee...so the robot is coming back at: [x: %f, y: %f]", 51.16, 11.05);

        pub_goal_on_ros(51.16, 11.05);
        returning=1;
    }
    else{ // l'utente non vuole più bere il secondo caffè e lo invia a uno degli utenti già loggati nel sistema
        other_target_position[0]=x;
        other_target_position[1]=y;
        ROS_INFO("User decides to drink just 1 coffee...so we are sending it to the other office: [x: %f, y: %f]", x, y);

        pub_goal_on_ros(x,y);
        goingOtherGoal=1;
    }
    cruising=1;

}

void CurrentPosCallback(const nav_msgs::OdometryConstPtr& pos){ // ci serve solo per capire se il nostro robot è vicino/arrivato al goal

    int ris = tfBuffer.canTransform("map", pos->header.frame_id, ros::Time(0));
    if (!ris) {
        ROS_ERROR("we cannot transform from /map to /odom frame!!!"); 
        return;
    }

    geometry_msgs::TransformStamped transform;
    transform = tfBuffer.lookupTransform("map", pos->header.frame_id, ros::Time(0));

    current_position[0]=pos->pose.pose.position.x + transform.transform.translation.x;  
    current_position[1]=pos->pose.pose.position.y + transform.transform.translation.y;
                               
}

void check(const ros::TimerEvent& event){
    if (cruising==1){

        if (returning == 1){
            float d_base=sqrt( pow(current_position[0]-base_position[0],2) + pow(current_position[1]-base_position[1],2));
            ROS_INFO("distance left from coffee machine is %f...", d_base);

            if (d_base < 1.5){
                ROS_INFO("ARRIVED TO THE COFFEE MACHINE");

                std_msgs::Int8 m;
                m.data = 2;
                arrived_pub.publish(m);
                // inizializzo tutto
                cruising=0;
                returning=0;
                goingOtherGoal=0;
                numCoffee=0;
                current_position[0]=0; current_position[1]=0;
                target_position[0]=0; target_position[1]=0;
                other_target_position[0]=0; other_target_position[1]=0;

            }
        }
        else if (goingOtherGoal==1) {
            float d_other=sqrt( pow(current_position[0]-other_target_position[0],2) + pow(current_position[1]-other_target_position[1],2));
            ROS_INFO("distance left from the other office is %f...", d_other);

            if (d_other < 1.7){
                ROS_INFO("ARRIVED TO THE OTHER OFFICE");

                std_msgs::Int8 m;
                m.data = 3;
                arrived_pub.publish(m);

                ros::Duration(5).sleep();
                pub_goal_on_ros(51.16, 11.05);
                returning=1;
                goingOtherGoal=0;
            }
        }
        else {
            float d=sqrt( pow(current_position[0]-target_position[0],2) + pow(current_position[1]-target_position[1],2));
            ROS_INFO("distance left from the office is %f...", d);

            if (d < 1.7){
                ROS_INFO("ARRIVED TO THE OFFICE");
            
                std_msgs::Int8 m;
                m.data = 1;
                arrived_pub.publish(m);

                if (numCoffee == 1){
                    ros::Duration(5).sleep();
                    pub_goal_on_ros(51.16, 11.05);
                    returning=1;
                }
                // altrimenti non fare niente (aspettiamo che lato utente ci incichi se dobbiamo ritornare alla macchinetta oppure andare ad un altro ufficio)
                else {cruising=0;}
            }
        } 
    }
}

int main(int argc, char** argv){
    
    ros::init(argc, argv, "reach_goal_node");
    ros::NodeHandle nh;

    tf2_ros::TransformListener tfListener(tfBuffer);

    pub = nh.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1000); 
    goal_sub = nh.subscribe("/reach_goal/goal", 1000, GoalCallback);
    pos_sub = nh.subscribe("/odom", 1000, CurrentPosCallback);

    arrived_pub = nh.advertise<std_msgs::Int8>("/reach_goal/arrived", 1000);  // credo che in questo modo ci può creare un topic che prima non esisteva
    other_goal_sub = nh.subscribe("/reach_goal/other_goal", 1000, OtherGoalCallback);

    // con ROS riesco a impostare una chiamata a una callback ogni certo intervallo di tempo
    ros::Timer t1 = nh.createTimer(ros::Duration(1), check);

    ros::Rate loop_rate(1);
    while (ros::ok()){
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}